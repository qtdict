TEMPLATE = lib
VERSION = 1.0
QT =core \
    network
CONFIG += qt \
    debug \
    warn_on \
    thread \
    plugin
DEFINES -= QT_NO_DEBUG_OUTPUT
win32:CONFIG *= windows
DESTDIR = bin
OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build
INCLUDEPATH += src/ \
    src/plugins
HEADERS = src/plugins/googleplugin.h \
    src/plugins/dictionaryinterface.h \
    src/parser.h \
    src/datahash.h \
    src/downloader.h \
    src/languages.h
SOURCES = src/plugins/googleplugin.cpp \
    src/parser.cpp \
    src/datahash.cpp \
    src/downloader.cpp
TARGET = googleplugin
DESTDIR = bin/plugins
