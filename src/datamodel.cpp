/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This source file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "datamodel.h"
#include <QtDebug>


DataModel::DataModel(QObject *parent): QAbstractItemModel(parent){
    //	qDebug() << "In function "+QString(Q_FUNC_INFO);
    rootNode = new TreeNode();
    num=0;
}
void DataModel::setData(const DataHashList& list){
    //	qDebug() << "In function "+QString(Q_FUNC_INFO);
    
    
    //QBrush headerbg=QBrush(QColor(150, 150, 150, 255));
    //QBrush headerfg=QBrush(QColor(0, 0, 100, 255));
    //QBrush wordbg=QBrush(QColor(150, 150, 150, 255));
    //QBrush wordfg=QBrush(QColor(0, 0, 100, 255));

    TreeNode *dict;

    for (int i = 0; i < list.size(); ++i) {
        qDebug() << "for: " + i;
        dict = new TreeNode(TreeNode::OnlineDictionary, list[i].getName());
        HashIterator it(list[i]);
        //list[i].debug();
        while(it.hasNext()){
            qDebug() << "while: " + it.hasNext();
            it.next();
            TreeNode *word = new TreeNode(TreeNode::Word, it.key());
            word->setParent(dict);
            QStringList meList=it.value();
            num+=meList.size();
            for (int m = 0; m < meList.size(); ++m) {
                qDebug() << "inlineFor: " + m;
                qDebug()<< m;
                TreeNode * meaning = new TreeNode(TreeNode::Meaning, meList[m]);
                meaning->setParent(word);
                word->addChild(meaning);
            }

            dict->addChild(word);
        }//while
        dict->setParent(rootNode);
        rootNode->addChild(dict);
    }
}

QModelIndex DataModel::index(int row, int column, const QModelIndex &parent) const{
    if (!rootNode)
        return QModelIndex();
    TreeNode *parentNode = nodeFromIndex(parent);
    return createIndex(row, column, parentNode->getChild(row));
}

TreeNode* DataModel::nodeFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        return static_cast<TreeNode*>(index.internalPointer());
    } else {
        return rootNode;
    }
}


QVariant DataModel::data(const QModelIndex &index, int role) const{
    if (role != Qt::DisplayRole)
        return QVariant();
    TreeNode *node = nodeFromIndex(index);
    if (!node)
        return QVariant();
    if (index.column() == 0) {
        switch (node->getType()) {
        case TreeNode::OnlineDictionary:
            //Do something for OnlineDictionary
            return "["+node->getText()+"]";
        case TreeNode::OfflineDictionary:
            //Do something for OfflineDictionary
            return node->getText();
        case TreeNode::Word:
            //Do something for Word
            return node->getText();
        default: //Sentinel
            return tr("");
        }
    } else if (index.column() == 1) {
        switch (node->getType()) {
                case TreeNode::Meaning:
            //Do something for Meaning
            return node->getText();
        default: //Sentinel
            return tr("");
        }
    }
    return QVariant();
}

QVariant DataModel::headerData(int section,
                               Qt::Orientation orientation,
                               int role) const{
    //qDebug() << "In function "+QString(Q_FUNC_INFO);
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        if (section == 0) {
            return tr("Szó");
        } else if (section == 1) {
            return tr("Jelentés");
        }
    }
    return QVariant();
}

int DataModel::wordCount(){
    return num;
}

int DataModel::rowCount(const QModelIndex &parent) const{
    TreeNode *parentNode = nodeFromIndex(parent);
    if (!parentNode)
        return 0;
    return parentNode->childCount();
}


int DataModel::columnCount(const QModelIndex &/*parent*/) const{
    return 2;
}

QModelIndex DataModel::parent(const QModelIndex &child) const{
    //qDebug() << "In function "+QString(Q_FUNC_INFO);
    TreeNode *node = nodeFromIndex(child);
    if (!node)
        return QModelIndex();
    TreeNode *parentNode = node->getParent();
    if (!parentNode)
        return QModelIndex();
    TreeNode *grandparentNode = parentNode->getParent();
    if (!grandparentNode)
        return QModelIndex();
    int row = grandparentNode->getChildren().indexOf(parentNode);
    return createIndex(row, child.column(), parentNode);
}

bool DataModel::hasChildren ( const QModelIndex & parent ){
    TreeNode *node=nodeFromIndex(parent);
    return node->hasChildren();
}

void DataModel::clear(){
    rootNode = new TreeNode();
    num=0;
}
