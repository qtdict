#include "selectionwidget.h"

SelectionWidget::SelectionWidget(const DicHash *init, QWidget *parent): QWidget(parent){
    qDebug() << "In function "+QString(Q_FUNC_INFO);
    setupUi(this);

    setAccessibleName(tr("Szótár választása"));

    refresh(init);
    hide();
}

void SelectionWidget::on_refreshButton_clicked(){
    qDebug() << "In function "+QString(Q_FUNC_INFO);
    emit refreshRequest();
}

void SelectionWidget::on_saveButton_clicked(){
    qDebug() << "In function "+QString(Q_FUNC_INFO);    
    QHash< QByteArray, bool> conf;
    QHashIterator<QByteArray, QCheckBox* > iter(checkBoxes);
    while(iter.hasNext()){
        iter.next();
        conf[ iter.key() ] = iter.value() -> isChecked();
    }
    emit configRequest(conf);
}

void SelectionWidget::refresh( const DicHash *hash ){    
    qDebug() << "In function "+QString(Q_FUNC_INFO);

    if (!hash){
        qDebug()<< QString(Q_FUNC_INFO)+": OOOPS, hash is empty! Exiting...";
        return;
    }

    qDeleteAll(labels);
    qDeleteAll(checkBoxes);

    DicHash::const_iterator iter = hash->begin();

    while (iter != hash->end()) {
        labels[iter.key()] = new QLabel( iter.value()->description(), this );
        labels[iter.key()] -> setWordWrap(true);

        checkBoxes[iter.key()] = new QCheckBox(iter.value()->toString(), this);
        checkBoxes[iter.key()] -> setChecked(iter.value()->isChecked());

        formLayout->addRow( checkBoxes[iter.key()], labels[iter.key()] );

        ++iter;
    }

}

void SelectionWidget::setup( const QHash< QByteArray, bool>& conf ){
    qDebug() << "In function "+QString(Q_FUNC_INFO);
    QHashIterator<QByteArray, bool > iter(conf);
    while(iter.hasNext()){
        iter.next();
        checkBoxes[ iter.key() ] -> setChecked( iter.value() );
    }
}

