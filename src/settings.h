/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This header file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef SETTINGS_H
#define SETTINGS_H

#include "utils.h"
#include <QSettings>
#include <QDir>

#ifdef Q_OS_WIN32
#include <shlobj.h>
#endif


#warning "TODO: implement DBSettings"

/*
class DBSettings{

};
*/

/*!
        State class for storing the state of a dictionary search instance
*/
class State{

public:
    //!( search term, dictionary language pair, mode index, database settings)
    State(){}
    State(const QString &w, const LangPair &lp, int mi  /*,const DBSettings&*/):word(w),dict(lp),mode(mi){}

    QString word;
    LangPair dict;
    int mode;

    bool operator==(const State &st) {
        return (st.dict==dict) && (st.word==word) && (st.mode==mode);
    }

};


class Settings{
private:
    QSettings *qsts;

public:

    Settings(QObject * parent);
    QSettings* getPtr(){return qsts;}

};

typedef QList<Settings> SettingsList;
typedef QList<State> StateList;
typedef QListIterator<State> StateListIterator;

#endif //SETTINGS_H
