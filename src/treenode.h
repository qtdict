/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This header file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __TREENODE_H__
#define __TREENODE_H__
#include <QString>
#include <QList>
#include <QtDebug>


class TreeNode{
public:
	enum Type { OnlineDictionary, OfflineDictionary, Word, Meaning, Sentinel };
	
	TreeNode(Type typ = Sentinel, const QString &txt = "");
	~TreeNode();
	
	Type getType();
	QString getText();
        TreeNode* getParent();
	QList<TreeNode *> getChildren();
	TreeNode* getChild(int row);
	bool hasChildren();
	int childCount();

	void setType(Type typ=Sentinel);
	void setText(QString text);
	void setParent(TreeNode *prt);
	int setChildren(QList<TreeNode *> chld);
	int addChild(TreeNode *chld);
	

private:

    Type type;
    QString str;
    TreeNode *parent;
    QList<TreeNode *> children;

};

#endif // __TREENODE_H__
