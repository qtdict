/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This header file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __LANGUAGES_H__
#define __LANGUAGES_H__

#include <QtDebug>
#include <QtCore>


//! language code list
enum Lang {
    AAA, HUN, ALB, ARA, BGR, CAT, CIG,
    CZH, DEN, ENG, ESP, EST, FIN, FLP,
    FRA, GAL, GER, GRE, HEB, HIN, HRV,
    IND, ITA, JAP, KOR, LAT, LIT, LTV,
    MAL, NED, NOR, POL, POR, ROM, RUS,
    SER, SLK, SLV, SWE, THI, TUR, UKR,
    VIE, ZHCN, ZHTW
};

typedef QHash< Lang, QString > LangNameHash;

class Languages{
    private:
            LangNameHash langNames;


    public:
	    Languages(){
QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
	     langNames.insert(AAA,QObject::tr("Nyelvfelismerés"));
	     langNames.insert(HUN,QObject::tr("Magyar"));
             langNames.insert(ALB,QObject::tr("Albán"));
             langNames.insert(ARA,QObject::tr("Arab"));
             langNames.insert(BGR,QObject::tr("Bolgár"));
             langNames.insert(CAT,QObject::tr("Katalán"));
             langNames.insert(CIG,QObject::tr("Cigány"));
             langNames.insert(CZH,QObject::tr("Cseh"));
             langNames.insert(DEN,QObject::tr("Dán"));
             langNames.insert(ENG,QObject::tr("Angol"));
             langNames.insert(ESP,QObject::tr("Spanyol"));
             langNames.insert(EST,QObject::tr("Észt"));
             langNames.insert(FIN,QObject::tr("Finn"));
	     langNames.insert(FLP,QObject::tr("Filippínó"));
             langNames.insert(FRA,QObject::tr("Francia"));
	     langNames.insert(GAL,QObject::tr("Galíciai"));
             langNames.insert(GER,QObject::tr("Német"));
             langNames.insert(GRE,QObject::tr("Görög"));
             langNames.insert(HEB,QObject::tr("Héber"));
             langNames.insert(HIN,QObject::tr("Hindi"));
             langNames.insert(HRV,QObject::tr("Horvát"));
	     langNames.insert(IND,QObject::tr("Indonéz"));
             langNames.insert(ITA,QObject::tr("Olasz"));
             langNames.insert(JAP,QObject::tr("Japán"));
             langNames.insert(KOR,QObject::tr("Koreai"));
             langNames.insert(LAT,QObject::tr("Latin"));
             langNames.insert(LIT,QObject::tr("Litván"));
             langNames.insert(LTV,QObject::tr("Lett"));
             langNames.insert(MAL,QObject::tr("Máltai"));
             langNames.insert(NED,QObject::tr("Holland"));
             langNames.insert(NOR,QObject::tr("Norvég"));
             langNames.insert(POL,QObject::tr("Lengyel"));
             langNames.insert(POR,QObject::tr("Portugál"));
             langNames.insert(ROM,QObject::tr("Román"));
             langNames.insert(RUS,QObject::tr("Orosz"));
             langNames.insert(SER,QObject::tr("Szerb"));
             langNames.insert(SLK,QObject::tr("Szlovák"));
             langNames.insert(SLV,QObject::tr("Szlovén"));
             langNames.insert(SWE,QObject::tr("Svéd"));
             langNames.insert(THI,QObject::tr("Thai"));
             langNames.insert(TUR,QObject::tr("Török"));
             langNames.insert(UKR,QObject::tr("Ukrán"));
             langNames.insert(VIE,QObject::tr("Vietnami"));
             langNames.insert(ZHCN,QObject::tr("Kínai (egysz.)"));
             langNames.insert(ZHTW,QObject::tr("Kínai (trad.)"));
	    }

            inline QString lang2Str(const Lang &lng){
		return langNames.value(lng);
	    }

            inline QString lang2Str(int lng){
                return langNames.value((Lang)lng);
            }

            inline Lang str2Lang(const QString &lng){
                return langNames.key(lng);
            }
};

#endif //__LANGUAGES_H__
