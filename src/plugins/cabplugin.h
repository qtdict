 #ifndef __CABPLUGIN_H__
 #define __CABPLUGIN_H__

 #include "dictionaryinterface.h"
 #include "../downloader.h"

 #include "../parser.h"

class CabPlugin : public DictionaryInterface
{
    Q_OBJECT
    Q_INTERFACES(DictionaryInterface)
private:
    QByteArray ID;

protected:
    QueryHash query;
    DataHash hash;
    LangHash supportedLangs;
    StrPairList latinToOldHex;
    StrPairList htmlToLatin;
    Downloader *downloader;
    QString searchKey;
    bool checked;
    bool dir;
    QUrl url;
    Languages lang;

public:
    CabPlugin();
    ~CabPlugin(){
        delete downloader;
    }

    bool search(const State &);

    QString toString(){
        return "Cab";
    }

    const QByteArray id() const{
        return ID;
    }

    const LangHash getLangHash() const;
    bool isChecked();
    bool isMchTranslator(){
        return false;
    }
    QString description();

public slots:
    void setChecked( bool );
    void catchError( const QString & );
    void finished(const QStringList& );

};

#endif //__CABPLUGIN_H__
