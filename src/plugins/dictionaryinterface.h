/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This header file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __DICTIONARYINTERFACE_H__
#define __DICTIONARYINTERFACE_H__

#include <QtPlugin>
#include <QtCore>

#include "../datahash.h"
#include "../settings.h"


/*!
        Interface for dictionary plugins
*/

class DictionaryInterface: public QObject {
    Q_OBJECT
public:
    DictionaryInterface(): QObject() {}
    virtual ~DictionaryInterface() {}

    virtual const QByteArray id() const = 0;
    virtual const LangHash getLangHash() const = 0;

    //!returned String has to be a unique identifier
    virtual QString toString() = 0;
    virtual QString description() = 0;
    virtual bool search(const State&) = 0;
    virtual bool isChecked() = 0;
    virtual bool isMchTranslator()=0;

public slots:
    virtual void setChecked(bool opt) = 0;
    virtual void finished(const QStringList& ) = 0;
    virtual void catchError(const QString & ) = 0;

signals:
    //!return the downloaded data, error indicator, and hostname
    void done(DataHash& data, const QString& host);
    void errorOccured(const QString&);

};
typedef QHash< QByteArray, DictionaryInterface* > DicHash;
typedef QHashIterator<QByteArray, DictionaryInterface* > DicHashIterator;

Q_DECLARE_INTERFACE(DictionaryInterface,"cc.oos.qtdict.DictionaryInterface/1.0")
#endif // __DICTIONARYINTERFACE_H__
