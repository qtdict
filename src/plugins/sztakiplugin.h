 #ifndef __SZTAKIPLUGIN_H__
 #define __SZTAKIPLUGIN_H__

 #include "dictionaryinterface.h"
 #include "../downloader.h"

 #include "../parser.h"

class SztakiPlugin : public DictionaryInterface
{
    Q_OBJECT

    Q_INTERFACES(DictionaryInterface)
private:
    QByteArray ID;

protected:
    QueryHash query;
    DataHash hash;
    LangHash supportedLangs;
    StrPairList latinToOldHex;
    StrPairList htmlToLatin;
    Downloader *downloader;
    QString searchKey;
    bool checked;
    QUrl url;
    Languages lang;

public:

    SztakiPlugin();
    ~SztakiPlugin(){
        delete downloader;
    }

    bool search(const State &);

    QString toString(){
        return "Sztaki";
    }

    const QByteArray id() const{
        return ID;
    }

    const LangHash getLangHash() const;
    bool isChecked();
    bool isMchTranslator(){
        return false;
    }
    QString description();

public slots:
    void setChecked( bool );
    void catchError( const QString & );
    void finished(const QStringList& );

};

#endif //__SZTAKIPLUGIN_H__
