 #ifndef __GOOGLEPLUGIN_H__
 #define __GOOGLEPLUGIN_H__

 #include "dictionaryinterface.h"
 #include "../downloader.h"

 #include "../parser.h"

class GooglePlugin : public DictionaryInterface
{
    Q_OBJECT

    Q_INTERFACES(DictionaryInterface)
private:
    QByteArray ID;

    // needed for single language dictionary because of different parsing
    bool singleMode;

protected:
    QueryHash query;
    DataHash hash;
    LangHash supportedLangs;
    StrPairList latinToOldHex;
    StrPairList htmlToLatin;
    Downloader *downloader;
    QString searchKey;
    bool checked;
    QUrl url;
    Languages lang;

public:

    GooglePlugin();
    ~GooglePlugin(){
        delete downloader;
    }

    bool search(const State &);

    QString toString(){
        return "Google";
    }

    const QByteArray id() const{
        return ID;
    }

    const LangHash getLangHash() const;
    bool isChecked();
    bool isMchTranslator(){
        return false;
    }
    QString description();

public slots:
    void setChecked( bool );
    void catchError( const QString & );
    void finished(const QStringList& );

};

#endif //__GOOGLEPLUGIN_H__
