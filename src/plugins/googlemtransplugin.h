 #ifndef __GOOGLEMTRANSPLUGIN_H__
 #define __GOOGLEMTRANSPLUGIN_H__

 #include "dictionaryinterface.h"
 #include "../downloader.h"

 #include "../parser.h"

class GoogleMTransPlugin : public DictionaryInterface
{
    Q_OBJECT

    Q_INTERFACES(DictionaryInterface)
private:
    QByteArray ID;

    // needed for single language dictionary because of different parsing
    bool singleMode;

protected:
    QueryHash query;
    DataHash hash;
    LangNameHash supportedLangs;
    StrPairList latinToOldHex;
    StrPairList htmlToLatin;
    Downloader *downloader;
    QString searchKey;
    bool checked;
    QUrl url;
    Languages lang;

public:

    GoogleMTransPlugin();
    ~GoogleMTransPlugin(){
        delete downloader;
    }

    bool search(const State &);

    QString toString(){
        return "Google";
    }

    const QByteArray id() const{
        return ID;
    }

    const LangHash getLangHash() const;
    bool isChecked();
    bool isMchTranslator(){
        return true;
    }
    QString description();

public slots:
    void setChecked( bool );
    void catchError( const QString & );
    void finished(const QStringList& );

};

#endif //__GOOGLEMTRANSPLUGIN_H__
