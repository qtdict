#include "sztakiplugin.h"

SztakiPlugin::SztakiPlugin(){
    QString seed = this->toString();
    ID = QCryptographicHash::hash( seed.toAscii(), QCryptographicHash::Md5);

    downloader = new Downloader(this);
    checked = true;

    connect(downloader, SIGNAL(errorOccured(const QString &)),
            this, SLOT(catchError(const QString&)));

    //>>>>>>>>
    //! connecting the downloader.done() signal to member slot finished()
    connect(downloader, SIGNAL(done(const QStringList&)),
            this, SLOT(finished(const QStringList&)));
    //<<<<<<<<

    searchKey=QString("W");
    url.setHost("szotar.sztaki.hu");
    url.setPath("/dict_search.php");

    QStringList queryLang( QStringList()
                           << "ENG:HUN:EngHunDict" << "HUN:ENG:EngHunDict" << "GER:HUN:GerHunDict"
                           << "HUN:GER:GerHunDict" << "FRA:HUN:FraHunDict" << "HUN:FRA:FraHunDict"
                           << "ITA:HUN:ItaHunDict" << "HUN:ITA:ItaHunDict" << "HOL:HUN:HolHunDict"
                           << "HUN:HOL:HolHunDict" << "POL:HUN:PolHunDict" << "HUN:POL:PolHunDict"
                           );

    LangPairList lplst = ( LangPairList()
                           << qMakePair(ENG, HUN) << qMakePair(HUN, ENG)
                           << qMakePair(GER, HUN) << qMakePair(HUN, GER)
                           << qMakePair(FRA, HUN) << qMakePair(HUN, FRA)
                           << qMakePair(ITA, HUN) << qMakePair(HUN, ITA)
                           << qMakePair(NED, HUN) << qMakePair(HUN, NED)
                           << qMakePair(POL, HUN) << qMakePair(HUN, POL)
                           );

    int i=0;
    foreach( LangPair pair, lplst ) {
        //!populating LangHash
        supportedLangs[pair] = lngConv.lang2Str(pair.first) +"-"+ lngConv.lang2Str(pair.second) ;

        StrPairList lst;
        lst << StringPair( "L" , queryLang[i] ) << StringPair( "M" , "3" );
        query.insert( pair, lst );
        ++i;
    }

}


bool SztakiPlugin::search(const State &s) {

    if (query.contains(s.dict)) {
        StrPairList actQuery = query[s.dict];

        switch (s.mode) {
        case 0:
            actQuery.last().second="3";
            break;
        case 1:
            actQuery.last().second="1";
            break;
        case 2:
            actQuery.last().second="0";
            break;
        case 3:
            actQuery.last().second="2";
            break;
        default:
            actQuery.last().second="2";
            break;
        }

        actQuery.prepend( qMakePair( searchKey, s.word ) );
        actQuery.append( qMakePair(QString("C"),QString("1")));
        actQuery.append( qMakePair(QString("A"),QString("1")));
        qDebug() <<"ACTQUERY: "<< actQuery;

        url.setQueryItems(actQuery);
        url.setScheme("http");
        qDebug()<<"DICTIONARIES_URL: "<<url.toString();

        downloader->setUrl(url);
        downloader->download();

        return true;

    } else {
        return false;
    }
}

void SztakiPlugin::finished(const QStringList &lst ) {
    qDebug() << "In function "+QString(Q_FUNC_INFO);


    QList< QRegExp > rxlist;
    QRegExp href("<a href='.+'>");
    QRegExp enda("</a>");
    QRegExp i("<i>");
    QRegExp endi("</i>");

    href.setMinimal(true);

    //*	A QRegExp list is used, to remove unneeded junk from result
    rxlist << href << enda << i << endi;

    //*	&lst is a QStringList of downloaded html sites, but we only downloaded one site, so we need lst.first()
    QString result = utils::remove( lst.first(), rxlist);

    QRegExp first("<!--m-->\\s(.+):\\s<!--/m-->");
    QRegExp second("<!--me-->\\s(.+\\s)<!--/me-->");
    first.setMinimal(true);
    second.setMinimal(true);

    Parser parser( result, first, second );
    hash=parser.parse("; ");

    //if ( !pairList.isEmpty() && pairList.last().second.split(" ").first() == "=>" ) pairList.removeLast(); // remove junk

    emit done( hash, url.host() );
}

const LangHash SztakiPlugin::getLangHash() const{
    return supportedLangs;
}

bool SztakiPlugin::isChecked(){
    return checked;
}

QString SztakiPlugin::description(){
    QStringList values=supportedLangs.values();
    return values.join(", ");
}

void SztakiPlugin::setChecked(bool opt){
    checked = opt;
}

void SztakiPlugin::catchError(const QString &msg ){
    emit errorOccured( msg );
}



Q_EXPORT_PLUGIN2(sztakiplugin, SztakiPlugin)
