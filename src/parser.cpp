/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This source file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "parser.h"

void Parser::setRegExp(const QRegExp &rxLeft, const QRegExp &rxRight){
    qDebug() << "In function "+QString(Q_FUNC_INFO);
    left=rxLeft;
    right=rxRight;
}

void Parser::setText(const QString &text){
    string=text;
}

QString Parser::getString(){
    return string;
}

QRegExp Parser::getLeft(){
    return left;
}

QRegExp Parser::getRight(){
    return right;
}


const DataHash Parser::parse(const QString &separator){
    qDebug() << "In function "+QString(Q_FUNC_INFO);
    QStringList fMatch;
    QStringList sMatch;
    DataHash hash;

    fMatch = utils::filter( string, left );
    sMatch = utils::filter( string, right );
    int size=0;
    qDebug()<<fMatch;
    qDebug()<<sMatch;

    if (separator=="1"){
        for (int i=1; i<sMatch.size();i++){
            fMatch<<fMatch.front();
        }        
    }

    if (fMatch.size() <= sMatch.size()) size=fMatch.size();
    else size=sMatch.size();

    if (fMatch.size() != sMatch.size() ) {qDebug() << "Size mismatch in function "+QString(Q_FUNC_INFO);}

    for (int i = 0; i < size; ++i) {
        QStringList tmp = sMatch[i].split( separator, QString::SkipEmptyParts );

        for (int j = 0; j < tmp.size(); ++j) {
            if( !tmp[j].contains("=>") )	hash.add( fMatch[i], tmp[j] );
            qDebug()<<"HASH_SIZE: "<<hash.size();
        }
    }
    return hash;
}

