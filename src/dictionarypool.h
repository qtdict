/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This header file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __DICTIONARYPOOL_H__
#define __DICTIONARYPOOL_H__

#include <QPluginLoader>
#include <QtPlugin>
#include <QDir>

#include "settings.h"
#include "plugins/dictionaryinterface.h"
#include "datamodel.h"
#include "utils.h"

class DictionaryPool: public QObject {
    Q_OBJECT
    
public:
    DictionaryPool(QObject*);

    ~DictionaryPool(){
        qDebug() << "In function "+QString(Q_FUNC_INFO);
        qDeleteAll(pool);
        qDebug("pool deleted");
        delete model;
        qDebug("model deleted");
    }

    void collect(const State &state, bool mTrans=false);

    const QHash< QByteArray, bool> getCurrentSelections();
    const DicHash* plugins();

    bool isEnabled(const QByteArray& );
    int add(DictionaryInterface* dic);
    int remove(DictionaryInterface* dic);
    void clear();
    void sync( bool selection=true );

private:
    int errCount;
    QString currentWord;

    QSet<DictionaryInterface*> pool;
    DicHash loadedPlugins;
    LangHash currentLangs;
    DataHashList data;
    DataModel *model;

    void setConnections();

    bool loadPlugins();
    void offlineDebug();

private slots:
    void finished(DataHash&, const QString& );
    void handleErrors(const QString & );

public slots:
    void config( const QHash< QByteArray, bool> &);
    void selectAll();
    bool refreshPlugins();

signals:    
    void errorOccured(const QString &, int type=0);
    void information(const QString &);
    void refreshRequest( const LangHash & );
    void refreshRequest( const DicHash*  );

    void tabAdditionRequest( DataModel*, const QString& );
};

#endif // __DICTIONARYPOOL_H__
