/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This source file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "qtdict.h"
#include <iostream>
using namespace std;
Qtdict::Qtdict() : QMainWindow(){

//!Application version
    appver=QString("0.5.0beta");
//<<version

cout<<"qtd1\n";

    settings = new Settings(this);
cout<<"qtd2\n";
    setWindowTitle("Qtdict");
cout<<"qtd3\n";
    setWindowIcon(QIcon(QString::fromUtf8(":/pic/dic_small.png")));
cout<<"qtd4\n";
    central = new CentralWidget(this);
cout<<"qtd5\n";
    setCentralWidget(central);
cout<<"qtd6\n";
    tray = new DictionaryTray(this);
    cout<<"qtd7\n";
    tray -> show();
    cout<<"qtd8\n";
    central->setTray(tray);
    cout<<"qtd9\n";
    createActions();
    cout<<"qtd10\n";
    createMenus();
    cout<<"qtd11\n";
    //createToolBars();
    setStatusBar(tr("Készenlét: <font color=green>Qtdict ")+appver);
    cout<<"qtd12\n";
    createConnections();
cout<<"qtd13\n";
    readSettings();
cout<<"qtd14\n";
}

Qtdict::~Qtdict(){
    qDebug() << "In function "+QString(Q_FUNC_INFO);
    delete settings;
    qDebug("settings deleted");
    delete central;
    qDebug("central deleted");
}

void Qtdict::createConnections() {


    connect(aboutAct, SIGNAL(triggered()),
            this, SLOT(about()));
    connect(newTabAct, SIGNAL(triggered()),
            central, SLOT(newTab()));
    connect(delHistoryAct, SIGNAL(triggered()),
            central, SLOT(clearHistory()));
    connect(dicSelectAct, SIGNAL(triggered()),
            central, SLOT(showSelectionWidget()));
    connect(inNewTabAct, SIGNAL(toggled(bool)),
            central, SLOT(setInNewTab(bool)));
    connect(central, SIGNAL(enableHistoryClearing(bool)),
            delHistoryAct, SLOT(setEnabled(bool)));
    connect(central, SIGNAL(information( const QString&, const QString&, const QString& )),
            this, SLOT(information( const QString&, const QString&, const QString& )));

    connect(exitAct, SIGNAL(triggered()),
            this, SLOT(close()));
    connect(licenceAct, SIGNAL(triggered()),
            central, SLOT(showLicence()));
}

void Qtdict::closeEvent(QCloseEvent *event) {
    qDebug() << "In function "+QString(Q_FUNC_INFO);
    writeSettings();
    event->accept();
}


//!	show information in the tableWidget area

void Qtdict::information(const QString &info, const QString &dict, const QString &word) {
    qDebug() << "In function "+QString(Q_FUNC_INFO);
    setStatusBar(tr("Keresett szó: ")+word+". "+info);
	//TODO: set colors;
        //QString color=(err?"\"red\"":"\"black\"");
        setWindowTitle(word+" - "+dict+ " - Qtdict" );
}

//! process errors which occured during download and data pocessing
/*
void Qtdict::error(const QString& msg) {
    qDebug() << "In function "+QString(Q_FUNC_INFO);

        QMessageBox::information(this, tr("HTTP"),
                                 tr("<font color=red>A letöltés meghiúsult:<br/> %1.")
                                 .arg(msg));

    information(tr("A letöltés meghiúsult:\n%1.")
                .arg(msg),
                true);
}
*/
void Qtdict::about() {
    QMessageBox::about(this,"Qtdict Információ", "<b>Qtdict v"+appver+"</b>"+tr("<br/><i>© Bécsi András, 2008-2009</i><br/><a href=\"http://qtdict.wordpress.com\">qtdict.wordpress.com</a><br/><br/>Egyszerű szótár program, mely jelenleg online szótárak adatbázisát használva teljesíti a lekérdezéseket. A szótár adatbázisokat beépülö modulként (plug-in) kezelni, ezáltal dinamikusan bővíthető és transzparens a szótár globális elhelyezkedését tekintve, azaz offline szótárak kezelésére is képes.<br/><br/><i>Ez a program a GPL v2 jogi feltételeinek megfelelően használható.</i>"));
}

/*
void Qtdict::updateUi() {
    central->lineEdit->setText(state.word);
    central->setCurrentLangPair(state.dict);
    central->modeBox->setCurrentIndex(state.mode);
}
*/

void Qtdict::on_delHistoryAct_triggered() {
    qDebug()<<"delHistory";
    statusBar()->showMessage(tr("Keresési előzmények törölve"),250);
    delHistoryAct->setEnabled(false);
    central->clearHistory();
}

void Qtdict::createActions() {

    exitAct = new QAction(QIcon(QString::fromUtf8(":/pic/exit.png")), tr("&Kilépés"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));
    exitAct->setStatusTip(tr("Kilépés a programból"));

    newTabAct = new QAction(QIcon(QString::fromUtf8(":/pic/tab_new.png")),tr("Új &lap"), this);
    newTabAct->setShortcut(tr("Ctrl+T"));
    newTabAct->setStatusTip(tr("Új lap megnyitása"));

    aboutAct = new QAction(QIcon(QString::fromUtf8(":/pic/info.png")),tr("Né&vjegy: Qtdict"), this);
    aboutAct->setStatusTip(tr("A Qtdict névjegyének megtekintése"));

    delHistoryAct = new QAction(QIcon(QString::fromUtf8(":/pic/delhist.png")), tr("Előzmények &törlése"), this);
    delHistoryAct -> setStatusTip(tr("Keresési előzmények törlése"));
    
    dicSelectAct = new QAction(QIcon(QString::fromUtf8(":/pic/configure.png")),tr("&Szótárak"), this);
    dicSelectAct -> setShortcut(tr("Ctrl+S"));
    dicSelectAct -> setStatusTip(tr("Használni kívánt szótárak kiválasztása"));

    inNewTabAct = new QAction(tr("&Megnyitás új lapon"),this);
    inNewTabAct -> setShortcut(tr("Ctrl+M"));
    inNewTabAct ->setCheckable(true);
    inNewTabAct ->setChecked(false);
    inNewTabAct ->setStatusTip(tr("Keresések megnyitása új lapon"));

    configAct = new QAction(QIcon(QString::fromUtf8(":/pic/settings.png")), tr("&Beállítások"), this);
    configAct -> setShortcut(tr("Ctrl+C"));
    configAct -> setStatusTip(tr("A program beállításai"));
    
    licenceAct = new QAction(tr("Li&cenc"), this);
    licenceAct -> setStatusTip(tr("A felhasználási feltételek megtekintése."));
}

void Qtdict::createMenus() {
    fileMenu = menuBar()->addMenu(tr("&Fájl"));
    /*
      fileMenu->addAction(newAct);
      fileMenu->addAction(openAct);
      fileMenu->addAction(saveAct);
      fileMenu->addAction(saveAsAct);
      fileMenu->addSeparator();
    */
    fileMenu->addAction(newTabAct);    
    fileMenu->addSeparator();
    fileMenu->addAction(delHistoryAct);
    fileMenu->addAction(exitAct);    
    fileMenu->setStatusTip(tr("Fájl"));


    editMenu = menuBar()->addMenu(tr("&Eszközök"));
    editMenu->setStatusTip(tr("A program beállításai"));
    editMenu->addAction(inNewTabAct);
    editMenu->addAction(dicSelectAct);
//    editMenu->addAction(configAct);

    helpMenu = menuBar()->addMenu(tr("&Segítség"));
    helpMenu->setStatusTip(tr("Segítség"));
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(licenceAct);
}



void Qtdict::setStatusBar(const QString& label) {
   statusBar()->showMessage(label,1000);
}



void Qtdict::readSettings() {
    QPoint pos = settings->getPtr()->value("pos", QPoint(200, 200)).toPoint();
    QSize size = settings->getPtr()->value("size", QSize(600, 600)).toSize();
    int from = settings->getPtr()->value("from", ENG).toInt();
    int to = settings->getPtr()->value("to", HUN).toInt();
    central->setCurrentLangPair(lngConv.lang2Str(from),lngConv.lang2Str(to));
    resize(size);
    move(pos);
}


void Qtdict::writeSettings() {
    settings->getPtr()->setValue("pos", pos());
    settings->getPtr()->setValue("size", size());
    settings->getPtr()->setValue("from",central->currentLangPair().first);
    settings->getPtr()->setValue("to",central->currentLangPair().second);
}
