/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This header file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef QTDICT_H
#define QTDICT_H


#include "settings.h"
#include "centralwidget.h"
#include "dictionarytray.h"
#include <QMainWindow>
#include <QtGui>

class QAction;
class QMenu;
class QTextEdit;

class Qtdict: public QMainWindow {
    Q_OBJECT

public:
    Qtdict();
    ~Qtdict();

protected:
    void closeEvent(QCloseEvent *event);

private slots:


    void information( const QString&, const QString&, const QString& );
  //  void error(const QString&);
    void on_delHistoryAct_triggered();
    void about();

private:
    void createActions();
    void createMenus();
    void createConnections();
    void setStatusBar(const QString&);
    void readSettings();
    void writeSettings();

    Settings *settings;

    QBrush headerbg;
    QBrush errorbg;
    QFont serifFont;
    QFont arialBold;
    DictionaryTray *tray;
    CentralWidget *central;

    QString stateFile;
    QString appver;
    QLabel *status;

    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *helpMenu;
    QAction *delHistoryAct;
    QAction *exitAct;
    QAction *newTabAct;


    QAction *aboutAct;
    QAction *configAct;
    QAction *dicSelectAct;
    QAction *licenceAct;
    QAction *inNewTabAct;

};

#endif
