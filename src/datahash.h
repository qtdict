/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This header file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __DATAHASH_H__
#define __DATAHASH_H__
#include <QHash>
#include <QStringListIterator>
 
typedef QHash<QString,QStringList> Hash;
class DataHash: public Hash{
public:
    DataHash(){}
    DataHash(const QString &name);
    DataHash(const QString &name, const QString &key, const QStringList &valueList);
    void add(const Hash &h);
    void add(const QString &key, const QString &meaning);
    void setName(const QString &name);
    void debug() const;
    QString getName() const;
    QStringListIterator getIterator(const QString &key);
 private:
    QString id;
};

typedef QList<DataHash> DataHashList;
typedef QHashIterator<QString, QStringList> HashIterator;

#endif // __DATAHASH_H__
