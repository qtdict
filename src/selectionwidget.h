#ifndef SELECTIONWIDGET_H
#define SELECTIONWIDGET_H

#include "utils.h"
#include "plugins/dictionaryinterface.h"
#include "ui_dicselect.h"
#include <QCheckBox>

class SelectionWidget: public QWidget, public Ui::DicSelect {
    Q_OBJECT
public:
    SelectionWidget(const DicHash*, QWidget *parent=0);

private:
    QHash< QByteArray, QLabel* > labels;
    QHash< QByteArray, QCheckBox* > checkBoxes;

private slots:
    void on_refreshButton_clicked();
    void on_saveButton_clicked();

public slots:
    void refresh( const DicHash* );
    void setup( const QHash< QByteArray, bool>& );

signals:
    void refreshRequest();
    void configRequest( const QHash< QByteArray, bool>& );
};


#endif // SELECTIONWIDGET_H
