/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This source file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "datahash.h"
#include <QtDebug>

DataHash::DataHash(const QString &name):Hash(),id(name){}

DataHash::DataHash(const QString &name, const QString &key, const QStringList &valueList):Hash(),id(name){
    //qDebug() << "In function "+QString(Q_FUNC_INFO);
    if (!contains(key)){
        qDebug() << QString(Q_FUNC_INFO)+": !contains(key)";
        insert(key, valueList);
    }else{
        qDebug("This DataHash contains a matching key \"%s\".",qPrintable(key));
    }
}

void DataHash::add(const Hash &h){
    //qDebug() << "In function "+QString(Q_FUNC_INFO);
    HashIterator it(h);
    while(it.hasNext()){
            qDebug() << "while: " + it.hasNext();
            it.next();
            for(int i=0;i<it.value().size();i++){
                add(it.key(),it.value()[i]);
            }
    }
}

void DataHash::add(const QString &key, const QString &value){
    //qDebug() << "In function "+QString(Q_FUNC_INFO);
    QStringList valueList;
    valueList=take(key);
    valueList.append(value);
    insert(key, valueList);
}

void DataHash::setName(const QString &name){
    id=name;
}

QString DataHash::getName() const{
    return id;
}
void DataHash::debug() const{
    qDebug()<<id;
    QList<QString> stringList=keys();
    QList<QStringList> list=values();
    for(int i=0;i<list.size();i++){
        qDebug()<<stringList[i]<<": "<<list[i];
    }
}

QStringListIterator DataHash::getIterator(const QString &key){
    return QStringListIterator(value(key));
}
