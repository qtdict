/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This header file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CENTRALWIDGET_H
#define CENTRALWIDGET_H

#include <QTreeView>
#include <QFile>
#include <QHttp>
#include <QByteArray>
#include <QBuffer>
#include <QHeaderView>
#include <QToolButton>
#include <QTextBrowser>
#include <QMessageBox>
#include <QCompleter>
#include <QPlainTextEdit>

#include "ui_qtdict.h"
#include "ui_information.h"
#include "datamodel.h"
#include "datahash.h"
#include "utils.h"
#include "dictionarypool.h"
#include "selectionwidget.h"
#include "dictionarytray.h"

class InfoWidget: public QWidget, public Ui::Information {
private:
    QPixmap icon;
    QLabel *iconLabel;
public:
    InfoWidget(QWidget *parent);
    void showText(const QString &str);
    void showIcon();
};

/*!
        centralwidget class for setting up central dictionary widget
*/

class CentralWidget: public QWidget, public Ui::Qtdict {
    Q_OBJECT
public:

    CentralWidget(QWidget *parent);
    ~CentralWidget();
    int tabCount();
    void removeTab(int);
    int indexOf(QWidget*);
    LangPair currentLangPair();
    void setCurrentIndex(int);    
    void setCurrentLangPair( const QString &from, const QString &to);
    void setTray(DictionaryTray*);


private:

    DictionaryTray* tray;
    DictionaryPool* manager;
    SelectionWidget *poolSelectionWidget;
    State currentState;
    DataHash languages;    

    QStringList history;
    QCompleter *completer;
    QPlainTextEdit *machineInput;
    QToolButton *crossButton;    
    DataModel *currentModel;
    QBuffer *data;
    QHttp* http;
    QString site;

    int histCap;
    int request;
    bool tabOption;
    bool mTrans;

    void saveState();
    void setConnections();

public slots:
    int addTab(DataModel *model, const QString &word);
    int addTab(QWidget *widget, bool option, const QString &title = "",  bool flag=false);
    void showInformation(const QString &str="", const QString &title = tr("Információ"));
    bool setInNewTab(bool option=true);
    void slotCloseCurrentTab();    
    void showSelectionWidget();
    void newTab();
    void clearHistory();
    void showLicence();
    void refreshLangBox(const LangHash &);
    void refreshToBox(const QString &);

private slots:
    void on_lineEdit_textEdited( const QString & );
    void on_pushButton_clicked( );
    void on_mTransButton_clicked();
    void on_switchButton_clicked();
    void handleDoubleClick ( const QModelIndex & );
    void slotMachineInputTextChanged();
    void handleErrors(const QString&);    
    void httpRequestFinished(int, bool);
    void finished(bool);

signals:    
    void information( const QString&, const QString&, const QString& );
    void machineInputChanged( const QString&);
    void enableHistoryClearing( bool);
};
#endif
