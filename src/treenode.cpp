/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This source file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "treenode.h"

TreeNode::TreeNode(Type typ, const QString &txt){
    type=typ;
    str=txt;
    parent=0;
}

TreeNode::~TreeNode(){
    qDebug() << "In function "+QString(Q_FUNC_INFO);
    qDeleteAll(children);
    delete parent;
}


TreeNode::Type TreeNode::getType(){
    return type;
}

QString TreeNode::getText(){
    return str;
}

TreeNode* TreeNode::getParent(){
    return parent;
}

QList<TreeNode *> TreeNode::getChildren(){
    return children;
}

bool TreeNode::hasChildren(){
    return (!children.isEmpty());
}

int TreeNode::childCount(){
    return children.size();
}

TreeNode* TreeNode::getChild(int row){
    if (!hasChildren())
        return (new TreeNode());
    else
        return children[row];
}

void TreeNode::setType(Type typ){
    this->type=typ;
}

void TreeNode::setText(QString text){
    this->str=text;
}

void TreeNode::setParent(TreeNode *prt){
    this->parent=prt;
}

int TreeNode::setChildren(QList<TreeNode *> chld){
    this->children=chld;
    return this->children.size();
}

int TreeNode::addChild(TreeNode *chld){
    this->children<<chld;
    return this->children.size();
}

