#ifndef DICTIONARYTRAY_H
#define DICTIONARYTRAY_H

#include <QSystemTrayIcon>

class QAction;

class DictionaryTray : public QSystemTrayIcon
{
	Q_OBJECT

	public:
                DictionaryTray( QWidget *parent = 0 );


	private:
		QAction *quitAct;
		QMenu *menu;
                QWidget *mainWindow;

        private slots:
                void action(QSystemTrayIcon::ActivationReason reason);

};

#endif //DICTIONARYTRAY_H
