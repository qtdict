/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This header file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include "settings.h"
#include <QHttp>
#include <QHttpResponseHeader>
#include <QBuffer>
#include <QUrl>

/*!
	Downloader class for downloading text based files from multiple urls
*/

class Downloader : public QObject {
    Q_OBJECT

public:
    Downloader(QObject *parent);
    ~Downloader();
    //!add url to urlList
    void addUrl(const QUrl &url);

    //!remove all occurences of URL from urlList
    void removeUrl(const QUrl &url);

    //!remove URL at index position i from urlList
    void removeUrlAt(int i);

    //!set url at index position i, the default for i is 0
    void setUrl(const QUrl &url,int i=0);
    //!start downloading from given URLs
    void download();

signals:
    //!done all downloads
    void done(const QStringList &);
    void errorOccured(const QString &);

private slots:
    void httpRequestFinished(int requestId, bool error);
    void readResponseHeader(const QHttpResponseHeader &responseHeader);
    void finished(bool error);

private:

    QString ERRORMSG;

    QList<QUrl> urlList;
    QStringList htmlList;

    QString codec;
    QHttp* http;
    QList<QBuffer*> bufferList;
    QList<int> idList;
};
#endif
