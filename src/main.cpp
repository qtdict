/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This source file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "qtdict.h"
#include <QTextCodec>
#include <QApplication>
#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
cout<<"Hello\n";
    Q_INIT_RESOURCE(qtdict);
    cout<<"Hello2\n";
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    cout<<"Hello3\n";
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    cout<<"Hello4\n";
    QApplication app(argc, argv);
    cout<<"Hello5\n";
    Qtdict win;
    cout<<"Hello6\n";
    win.show();
    cout<<"Hello7\n";
    app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
    cout<<"Hello8\n";
    return app.exec();
}
