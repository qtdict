/***************************************************************************
 *   Copyright (C) 2008 by András Bécsi                                    *
 *   andrewbecsi @ yahoo . co . uk                                         *
 *                                                                         *
 *   This header file is part of the Qtdict project.                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __PARSER_H__
#define __PARSER_H__

#include "datahash.h"
#include "utils.h"

class Parser{
        public:
    Parser();
    Parser(Parser &other):string(other.getString()),
    left(other.getLeft()),
    right(other.getRight()){}

    Parser(QRegExp &rxLeft, QRegExp &rxRight) : left(rxLeft),right(rxRight){}
    Parser(QString &text, QRegExp &rxLeft, QRegExp &rxRight):  string(text), left(rxLeft), right(rxRight){}

    void setRegExp(const QRegExp &rxLeft, const QRegExp &rxRight);
    void setText(const QString &text);

    QString getString();

    QRegExp getLeft();

    QRegExp getRight();


    const DataHash parse(const QString &separator);

        private:

    QString string;
    QRegExp left;
    QRegExp right;

};

#endif // __PARSER_H__
