// Copyright (c) 2008, Andrew Bécsi (andrewbecsi [at] yahoo [dot] co [dot] uk)
//
// This program is free software, released under the GNU General Public License
// and you are welcome to redistribute it under certain conditions.  It comes with
// ABSOLUTELY NO WARRANTY; for details read the GNU General Public License to be
// found in the file "COPYING" distributed with this program, or online at:
// http://www.gnu.org/copyleft/gpl.html

README Qtdict for Windows

A Qtdict egy Qt4 alapú nyílt forráskódú szótár program, mely online szótárakról interneten 
keresztül kérdezi le a keresett szavak jelentését. Ez a fájl win32 platformhoz készült.

További információkért letöltésekért, frissítésekért látogass el a 
http://bandix.extra.hu/qtdict honlapra.
-------------------------------------------------------------------------------------------
A következőkre csak fejlesztőknek van szüksége!
-------------------------------------------------------------------------------------------
A fordítás menete:
		C:\qtdict\> qmake -spec win32-g++ qtdict.pro
		C:\qtdict\> make.bat
(A make.bat fájlt a Qt/Win telepítőjével letölthető mingw környezet tartalmazza)
		
A telepítő fordítása:
A telepítő lefordításához a http://nsis.sourceforge.net oldalról letölthető, nyílt forráskódú
Nullsoft Scriptable Install System-re van szükség.
-------------------------------------------------------------------------------------------
FIGYELEM!
A fordításhoz szükséges a Qt4 for Windows valamely (>=4.2) verziója és a MinGW gcc fordítókörnyezet,
mely a Qt telepítővel letölthető. Ezen kívül természetesen használható a VS fordítója is.
A Qt4 a Qt3-as verziójával nem kompatibilis, a program 4.2-nél régebbi Qt verziókkal le sem fordul!

Ez a program a GPL v2 jogi feltételeinek megfelelően használható. 
(Bővebben lásd: COPYING vagy magyarul a http://www.gnu.hu/gpl.html címen)

(c) Bécsi András (andrewbecsi [at] yahoo [dot] co [dot] uk) 2008 - 2009
