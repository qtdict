TEMPLATE = lib
VERSION = 1.0
QT =core \
    network
CONFIG += qt \
    release \
    warn_on \
    thread \
    plugin
DEFINES -= QT_NO_DEBUG_OUTPUT
win32:CONFIG *= windows
DESTDIR = bin
OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build
INCLUDEPATH += src/ \
    src/plugins
HEADERS = src/plugins/cabplugin.h \
    src/plugins/dictionaryinterface.h \
    src/parser.h \
    src/datahash.h \
    src/downloader.h \
    src/languages.h
SOURCES = src/plugins/cabplugin.cpp \
    src/parser.cpp \
    src/datahash.cpp \
    src/downloader.cpp
TARGET = cabplugin
DESTDIR = bin/plugins
