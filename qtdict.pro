TEMPLATE = app
QT = core gui network
CONFIG += qt \
    release \
    warn_on \
    thread \
    plugin
DEFINES += QT_NO_DEBUG_OUTPUT
TARGET = qtdict

win32:CONFIG *= windows

DESTDIR = bin
OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build
RESOURCES = ui/qtdict.qrc
RC_FILE = ui/qtdict.rc
FORMS = ui/qtdict.ui \
    ui/dicselect.ui \
    ui/information.ui
HEADERS = src/utils.h \
    src/settings.h \
    src/qtdict.h \
    src/downloader.h \
    src/centralwidget.h \
    src/datahash.h \
    src/datamodel.h \
    src/parser.h \
    src/treenode.h \
    src/dictionarypool.h \
    src/dictionarytray.h \
    src/selectionwidget.h \
    src/languages.h \
    src/plugins/dictionaryinterface.h

SOURCES = src/qtdict.cpp \
    src/main.cpp \
    src/downloader.cpp \
    src/datahash.cpp \
    src/datamodel.cpp \
    src/parser.cpp \
    src/treenode.cpp \
    src/centralwidget.cpp \
    src/settings.cpp \
    src/dictionarypool.cpp \
    src/dictionarytray.cpp \
    src/selectionwidget.cpp 
    
