!define APPNAME "Qtdict"   ; Define application name
!define APPVERSION "0.5.0beta"  ; Define application version
!define INSTALLERVERSION 03 ; NEED TO UPDATE THIS FOR EVERY RELEASE!!!

!define ROOT "..\.."
!define PIC "${ROOT}\src\pic"

!define APPURLLINK "http://qtdict.wordpress.com"
!define APPNAMEANDVERSION "${APPNAME} ${APPVERSION}"
!define APPVERSIONINTERNAL "${APPVERSION}.0" ; Needs to be of the format X.X.X.X

!define MUI_ICON "${PIC}\qtdict.ico"
!define MUI_UNICON "${PIC}\uninstall.ico"
!define MUI_WELCOMEFINISHPAGE_BITMAP "${PIC}\welcome.bmp"
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "${PIC}\top.bmp"

BrandingText "Qtdict Installer"
SetCompressor LZMA

; Version Info
VIProductVersion "${APPVERSIONINTERNAL}"
VIAddVersionKey "ProductName" "Qtdict Installer"
VIAddVersionKey "Comments" "Installs ${APPNAMEANDVERSION}"
VIAddVersionKey "CompanyName" "Qtdict Developers"
VIAddVersionKey "FileDescription" "Installs ${APPNAMEANDVERSION}"
VIAddVersionKey "ProductVersion" "${APPVERSION}"
VIAddVersionKey "InternalName" "InstQtdict"
VIAddVersionKey "FileVersion" "${APPVERSION}"
VIAddVersionKey "LegalCopyright" " "
; Main Install settings
Name "${APPNAMEANDVERSION}"

; NOTE: Keep trailing backslash!
InstallDir "$PROGRAMFILES\Qtdict\"
InstallDirRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "Install Folder"
OutFile "${ROOT}\qtdict-${APPVERSION}-install.exe"
CRCCheck force

ShowInstDetails show
ShowUninstDetails show

Var SHORTCUTS

; Modern interface settings
!include "MUI.nsh"

!define MUI_ABORTWARNING

!insertmacro MUI_PAGE_WELCOME

!define MUI_LICENSEPAGE_RADIOBUTTONS
!insertmacro MUI_DEFAULT MUI_LICENSEPAGE_RADIOBUTTONS_TEXT_ACCEPT "&Elfogadom a licenszfelt�teleket"
!insertmacro MUI_DEFAULT MUI_LICENSEPAGE_RADIOBUTTONS_TEXT_DECLINE "&Nem fogadom el a licenszfelt�teleket"
!insertmacro MUI_PAGE_LICENSE "${ROOT}\COPYING.txt"

!insertmacro MUI_PAGE_DIRECTORY

;Start Menu Folder Page Configuration
!define MUI_STARTMENUPAGE_DEFAULTFOLDER $SHORTCUTS
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKEY_LOCAL_MACHINE"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Shortcut Folder"

!insertmacro MUI_PAGE_STARTMENU "Qtdict" $SHORTCUTS

!insertmacro MUI_PAGE_INSTFILES


!define MUI_FINISHPAGE_RUN "$INSTDIR\qtdict.exe"
!define MUI_FINISHPAGE_LINK "L�togass el a Qtdict honlapj�ra!"
!define MUI_FINISHPAGE_LINK_LOCATION "${APPURLLINK}"
!define MUI_FINISHPAGE_NOREBOOTSUPPORT
!define MUI_FINISHPAGE_SHOWREADME "$INSTDIR\README_WINDOWS.txt"
!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
!define MUI_WELCOMEFINISHPAGE_CUSTOMFUNCTION_INIT DisableBack

!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

; Set languages (first is default language)
!insertmacro MUI_LANGUAGE "Hungarian"
!insertmacro MUI_RESERVEFILE_LANGDLL

;--------------------------------------------------------------
; (Core) Qtdict install section. Copies all internal game data
Section "!Qtdict" Section1
	; Overwrite files by default, but don't complain on failure
	SetOverwrite try

	; Define root variable relative to installer
	!define PATH_ROOT "${ROOT}\"

	; Copy dll files
	SetOutPath "$INSTDIR\"
	File ${PATH_ROOT}\win\installer\dll\*.dll

	; Copy picture files
	SetOutPath "$INSTDIR\pic\"
	File ${PIC}\*.ico
	
	; Copy plugins
	SetOutPath "$INSTDIR\plugins"
	File ${PATH_ROOT}\release\installer\dll\*.dll
	
	; Copy text files
	SetOutPath "$INSTDIR\"
	File ${PATH_ROOT}\README_WINDOWS.txt
	File ${PATH_ROOT}\COPYING.txt

	; Copy executable
	File /oname=qtdict.exe ${PATH_ROOT}\src\release\bin\qtdict.exe

	; Create the Registry Entries
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "Comments" "Visit ${APPURLLINK}"
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "DisplayIcon" "$INSTDIR\qtdict.exe,0"
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "DisplayName" "Qtdict ${APPVERSION}"
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "DisplayVersion" "${APPVERSION}"
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "HelpLink" "${APPURLLINK}"
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "Install Folder" "$INSTDIR"
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "Publisher" "Qtdict"
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "Shortcut Folder" "$SHORTCUTS"
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "UninstallString" "$INSTDIR\uninstall.exe"
	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "URLInfoAbout" "${APPURLLINK}"
	; This key sets the Version DWORD that new installers will check against
	WriteRegDWORD HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "Version" ${INSTALLERVERSION}

	!insertmacro MUI_STARTMENU_WRITE_BEGIN "Qtdict"
	CreateShortCut "$DESKTOP\qtdict.lnk" "$INSTDIR\qtdict.exe"
	CreateDirectory "$SMPROGRAMS\$SHORTCUTS"
	CreateShortCut "$SMPROGRAMS\$SHORTCUTS\qtdict.lnk" "$INSTDIR\qtdict.exe"
	CreateShortCut "$SMPROGRAMS\$SHORTCUTS\uninstall.lnk" "$INSTDIR\uninstall.exe"
	CreateShortCut "$SMPROGRAMS\$SHORTCUTS\readme.lnk" "$INSTDIR\README_WINDOWS.txt"
	!insertmacro MUI_STARTMENU_WRITE_END
SectionEnd

;-------------------------------------------
; Install the uninstaller (option is hidden)
Section -FinishSection
	WriteUninstaller "$INSTDIR\uninstall.exe"
SectionEnd

; Modern install component descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${Section1} "A Qtdict egy Qt4 alap� ny�lt forr�sk�d� sz�t�r program, mely online sz�t�rakr�l interneten kereszt�l k�rdezi le a keresett szavak jelent�s�t."
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;-----------------------------------------------
; Uninstall section, deletes all installed files
Section "Uninstall"

	; Remove from registry...
	!insertmacro MUI_STARTMENU_GETFOLDER "Qtdict" $SHORTCUTS
	ReadRegStr $SHORTCUTS HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "Shortcut Folder"

	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict"
	
	; Delete pictures
	Delete "$INSTDIR\pic\*"
	RMDir "$INSTDIR\pic"		 
	Delete "$INSTDIR\*.dll"
	; Delete self
	Delete "$INSTDIR\uninstall.exe"

	; Delete Shortcuts
	Delete "$DESKTOP\qtdict.lnk"
	Delete "$SMPROGRAMS\$SHORTCUTS\qtdict.lnk"
	Delete "$SMPROGRAMS\$SHORTCUTS\uninstall.lnk"
	Delete "$SMPROGRAMS\$SHORTCUTS\readme.lnk"

	; Clean up Qtdict dir
	Delete "$INSTDIR\README_WINDOWS.txt"
	Delete "$INSTDIR\qtdict.exe"
	Delete "$INSTDIR\COPYING"
	Delete "$INSTDIR\INSTALL.LOG"


	; Remove remaining directories
	RMDir "$SMPROGRAMS\$SHORTCUTS\Extras\"
	RMDir "$SMPROGRAMS\$SHORTCUTS"
	RMDir "$INSTDIR"

SectionEnd

;----------------------------------------------------------------------------------
; Disable the "Back" button on finish page if the installer is run on Win9x systems
Function DisableBack
	Call GetWindowsVersion
	Pop $R0
	StrCmp $R0 "win9x" 0 WinNT
	!insertmacro MUI_INSTALLOPTIONS_WRITE "ioSpecial.ini" "Settings" "BackEnabled" "0"
WinNT:
	ClearErrors
FunctionEnd

;-------------------------------------------------------------------------------
; Determine windows version, returns "win9x" if Win9x/Me or "winnt" on the stack
Function GetWindowsVersion
	ClearErrors
	StrCpy $R0 "winnt"

	ReadRegStr $R1 HKLM "SOFTWARE\MICROSOFT\WINDOWS NT\CurrentVersion" CurrentVersion
	IfErrors 0 WinNT
	StrCpy $R0 "win9x"
WinNT:
	ClearErrors
	Push $R0
FunctionEnd

Var OLDVERSION
Var UninstallString

;-----------------------------------------------------------------------------------
; NSIS Initialize function, determin if we are going to install/upgrade or uninstall
Function .onInit
	StrCpy $SHORTCUTS "Qtdict"

	SectionSetFlags 0 17

	; Starts Setup - let's look for an older version of Qtdict
	ReadRegDWORD $R8 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "Version"

	IfErrors ShowWelcomeMessage ShowUpgradeMessage
ShowWelcomeMessage:
	ReadRegStr $R8 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "Version"
	IfErrors FinishCallback

ShowUpgradeMessage:
	IntCmp ${INSTALLERVERSION} $R8 VersionsAreEqual InstallerIsOlder  WelcomeToSetup
WelcomeToSetup:
	; An older version was found.  Let's let the user know there's an upgrade that will take place.
	ReadRegStr $OLDVERSION HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "DisplayVersion"
	; Gets the older version then displays it in a message box
	MessageBox MB_OK|MB_ICONINFORMATION \
		"�dv�z�l a ${APPNAMEANDVERSION} verzi�j�nak telep�t�je.$\n \
		A telep�t� seg�ts�g�vel friss�theted a $OLDVERSION verzi�j� programodat."
	Goto FinishCallback

VersionsAreEqual:
	ReadRegStr $UninstallString HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Qtdict" "UninstallString"
	IfFileExists "$UninstallString" "" FinishCallback
	MessageBox MB_YESNO|MB_ICONQUESTION \
		"A telep�t� megtal�lta a ${APPNAMEANDVERSION} verzi�j�t a rendszereden. Ez a telep�t� ezt a verzi�t telep�ti.$\n \
		El szeretn�d t�vol�tani a telep�tett verzi�t?" \
		IDYES DoUninstall IDNO FinishCallback
DoUninstall: ; You have the same version as this installer.  This allows you to uninstall.
	Exec "$UninstallString"
	Quit

InstallerIsOlder:
	MessageBox MB_OK|MB_ICONSTOP \
		"A rendszereden telep�tve van egy �jabb verzi� (${APPNAME}).$\n \
		A telep�t�s befejezve."
	Quit

FinishCallback:
	ClearErrors
FunctionEnd
; eof

